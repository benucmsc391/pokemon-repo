from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dropout, Flatten, Dense
from keras import backend as kb
from keras.models import load_model
from keras.preprocessing import image
# metrics
from sklearn import metrics
import numpy as np
import matplotlib.pyplot as plt
# for file management
import util.file_util as file_util
import math
import os


class PredictionData:
    """
    Prediction data wrapper class. Holds prediction class name, probability, class index, image path, and model labels.
    """

    def __init__(self, model, image_path: str, labels_directory: str, width: int, height: int, clear_session=False):
        """
        Creates a new PredictionData object for a particular image.
        :param model: A Keras Model or model path.
        :param image_path: Path to image.
        :param labels_directory: Directory containing class name directories.
        :param width: Image width.
        :param height: Image height.
        :param clear_session: Clear backend session.
        """
        model = get_model(model)

        img = image.load_img(image_path, target_size=(width, height))
        img_tensor = image.img_to_array(img)
        img_tensor = np.expand_dims(img_tensor, axis=0)

        class_indices = model.predict_classes(np.vstack([img_tensor]))

        img_tensor /= 255.

        class_predictions = model.predict(img_tensor, batch_size=1, verbose=1)[0]

        self.image_path = image_path
        self.class_index = class_indices[0]
        self.class_name = class_label(labels_directory, self.class_index)
        self.probability = max(class_predictions)
        self.labels = os.listdir(labels_directory)

        if clear_session:
            kb.clear_session()

    def __str__(self):
        """
        Creates a formatted string representation of the object.
        :return: Returns the formatted string.
        """
        return 'image_path: ' + self.image_path + ', class_name: ' + self.class_name + ', class_index: ' + str(
            self.class_index) + ', probability: ' + str(
            self.probability) + ', labels: ' + self.labels.__str__()


def save_model(model, file_name: str = None):
    """
    Saves a Keras model object to the keras_model directory.
    :param model: A Keras Model object.
    :param file_name: Optional name prefix. If no name used, will use current datetime instead.
    :return:
    """
    model_directory = 'keras_model/'
    weights_directory = 'keras_weights/'
    extension = '.h5'

    if file_name is None:
        file_name = file_util.date_string_now()

    model.save_weights(weights_directory + file_name + extension)
    model.save(model_directory + file_name + extension)


def get_model(model):
    """
    Utility function to load a Keras model. Allows a saved model file path or model object to be passed in.
    :param model: A Keras Model or model path.
    :return: Returns the loaded model if directory entered, else simply returns the passed in model object.
    """
    if type(model) == str:
        return load_model(model)
    elif type(model) == Sequential:
        return model


def _calc_steps_per_epoch(samples: int, batch_size: int):
    """
    Calculates steps per epoch based on sample count and batch size.
    :param samples: Number of samples.
    :param batch_size: Batch size.
    :return: Returns the ceiling of samples divided by batch size.
    """
    return math.ceil(samples / batch_size)


def filters_dropout_compensation(min_active_filters: int, dropout: float):
    """
    Calculate the minimum number of active filters adjusted for dropout. For example, if 50 active filters are
    required and a dropout value of .2 is used, the value 63 will be returned. (When the 63 filters are set, 20% will
    be turned off due to dropout).
    :param min_active_filters: The minimum number of active filters
    :param dropout: A dropout value
    :return: Returns a greater filter value to ensure the minimum number of filters are always active.
    """
    if dropout != 1:
        return int(math.ceil(min_active_filters / (1 - dropout)))
    else:
        return 0


def predictions_prepare_images(img_directory: str, width: int, height: int):
    """
    Converts images to a format usable for predictions.
    :param img_directory: Images to be loaded.
    :param width: Image width.
    :param height: Image height.
    :return: Returns the formatted images.
    """
    filenames = file_util.get_files_walk(img_directory)
    images = []

    for filename in filenames:
        img = image.load_img(filename, target_size=(width, height))
        img = image.img_to_array(img)
        img = np.expand_dims(img, axis=0)

        images.append(img)

    return np.vstack(images), filenames


def class_label(labels_directory: str, class_index: int):
    """
    Utility function to get a class label name.
    :param labels_directory: Parent directory of label directories.
    :param class_index: Index of name.
    :return: Returns the respective class name of the passed in class index.
    """
    return os.listdir(labels_directory)[class_index]


def get_predictions(model, img_directory: str, labels_directory: str, width: int, height: int, clear_session=False):
    """
    Gets prediction data from a passed in Keras model and image directory.
    :param model: A Keras Model or model path.
    :param img_directory: Directory path.
    :param labels_directory: Parent directory of labels directories.
    :param width: Image width.
    :param height: Image height.
    :param clear_session: Clear backend session.
    :return: Returns an array of PredictionData wrapper objects.
    """
    print('model:', model)
    print('image directory:', img_directory)

    # if model is file path string, load keras model from file path
    model = get_model(model)
    predictions = []

    image_paths = file_util.get_files_walk(img_directory)

    # get prediction data for each image
    for image_path in image_paths:
        prediction_data = PredictionData(model, image_path, labels_directory, width, height)
        predictions.append(prediction_data)
        print(prediction_data.__str__())

    if clear_session:
        kb.clear_session()

    return predictions


def model_metrics(model, test_directory: str, img_width: int, img_height: int):
    """
    Display precision, recall, F1-score, and support metrics of a particular model.
    :param model: A Keras Model or model path.
    :param test_directory: Validation images path.
    :param img_width: Image width.
    :param img_height: Image height.
    :return: Returns the metrics report.
    """
    model = get_model(model)

    test_generator = ImageDataGenerator()
    test_data_generator = test_generator.flow_from_directory(
        test_directory,
        target_size=(img_width, img_height),
        batch_size=1,
        class_mode=None,
        shuffle=False)

    test_steps_per_epoch = _calc_steps_per_epoch(test_data_generator.samples, test_data_generator.batch_size)

    predictions = model.predict_generator(test_data_generator, steps=test_steps_per_epoch)
    # Get most likely class
    predicted_classes = np.argmax(predictions, axis=1)

    true_classes = test_data_generator.classes
    class_labels = list(test_data_generator.class_indices.keys())

    report = metrics.classification_report(true_classes, predicted_classes, target_names=class_labels)
    print(report)

    return report


def show_plot(history, file_name: str = None):
    """
    Displays training/validation accuracy and loss values in two graphs.
    :param history: A history object.
    :param file_name: Pass a file name to save charts. Accuracy chart saved to 'graphs/accuracy', loss chart saved
    to 'graphs/loss'.
    :return: 
    """
    accuracy_directory = 'graphs/accuracy/'
    loss_directory = 'graphs/loss/'
    extension = '.png'

    fig_size = [8, 6]
    line_width = 2.0
    font_size = 16
    legend_font_size = 18

    # Plot the Loss Curves
    plt.figure(figsize=fig_size)
    plt.plot(history.history['loss'], 'r', linewidth=line_width)
    plt.plot(history.history['val_loss'], 'b', linewidth=line_width)
    plt.legend(['Training loss', 'Validation Loss'], fontsize=legend_font_size)
    plt.xlabel('Epochs ', fontsize=font_size)
    plt.ylabel('Loss', fontsize=font_size)
    plt.title('Loss Curves', fontsize=font_size)

    if file_name is not None:
        plt.savefig(loss_directory + 'loss-' + file_name + extension)
    plt.show()

    # Plot the Accuracy Curves
    plt.figure(figsize=fig_size)
    plt.plot(history.history['acc'], 'r', linewidth=line_width)
    plt.plot(history.history['val_acc'], 'b', linewidth=line_width)
    plt.legend(['Training Accuracy', 'Validation Accuracy'], fontsize=legend_font_size)
    plt.xlabel('Epochs ', fontsize=font_size)
    plt.ylabel('Accuracy', fontsize=font_size)
    plt.title('Accuracy Curves', fontsize=font_size)

    if file_name is not None:
        plt.savefig(accuracy_directory + 'accuracy-' + file_name + extension)
    plt.show()


def train(train_directory: str, validate_directory: str, img_width, img_height, save: bool = True, show: bool = True,
          seed=None):
    """
    Trains a Keras neural network to recognize image classes. Uses a combination of convolutional layers, max pooling
    layers, dense layers, and dropout.

    Note: train_directory and validate_directory must have the same class names for Keras to use them.

    :param train_directory: The parent directory of class directories containing training images.
    :param validate_directory: The parent directory of class directories containing validation images.
    :param img_width: Image width.
    :param img_height: Image height.
    :param save: Save to keras_model
    :param show: Show accuracy/loss plots after training.
    :param seed: Specify a seed value.
    :return:
    """
    # pre_processing_directory = None
    pre_processing_directory = 'img_preprocessing'

    # check preprocessing save directory is to be used
    if pre_processing_directory is not None:
        file_util.empty_directory(pre_processing_directory)

    rescale = 1. / 255
    width_shift_range = 0.
    height_shift_range = 0.
    shear_range = 0.
    zoom_range = 0.
    horizontal_flip = False
    fill_mode = 'nearest'

    # if using preprocessing, set values
    if pre_processing_directory is not None:
        width_shift_range = .05
        height_shift_range = .05
        shear_range = 2.
        zoom_range = .05
        horizontal_flip = True

    num_classes = file_util.count_subdirectories(train_directory)
    class_mode = 'categorical'

    batch_size = 32
    epochs = 12

    channels = 3
    kernel_size = (3, 3)
    pool_size = (2, 2)

    dropout = .4

    conv_2d_layers = 3
    conv_2d_filters = 128

    # ensure minimum number of active filters (adjust filters for dropout)
    # conv_2d_filters = filters_dropout_compensation(conv_2d_filters, dropout)
    print('conv_2d_filters:', conv_2d_filters)

    dense_filters = 512

    # ensure minimum number of active filters (adjust filters for dropout)
    # dense_filters = filters_dropout_compensation(dense_filters, dropout)
    print('dense_filters:', dense_filters, end='\n\n')

    # set input shape
    if kb.image_data_format() == 'channels_first':
        input_shape = (channels, img_width, img_height)
    else:
        input_shape = (img_width, img_height, channels)

    model = Sequential()

    # input layer
    model.add(Conv2D(conv_2d_filters, kernel_size, activation='relu', input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=pool_size))

    # add conv2d layers
    for i in range(conv_2d_layers):
        model.add(Conv2D(conv_2d_filters, kernel_size, activation='relu'))
        model.add(MaxPooling2D(pool_size=pool_size))
        conv_2d_filters = math.floor(conv_2d_filters * .67)

    # add flatten and dense layers
    model.add(Flatten())
    model.add(Dense(dense_filters, activation='relu'))
    model.add(Dropout(dropout))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    train_datagen = ImageDataGenerator(
        rescale=rescale,
        width_shift_range=width_shift_range,
        height_shift_range=height_shift_range,
        shear_range=shear_range,
        zoom_range=zoom_range,
        horizontal_flip=horizontal_flip,
        fill_mode=fill_mode,
    )

    validate_datagen = ImageDataGenerator(rescale=rescale)

    train_generator = train_datagen.flow_from_directory(
        train_directory,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=class_mode,
        save_to_dir=pre_processing_directory,
        seed=seed)

    validation_generator = validate_datagen.flow_from_directory(
        validate_directory,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=class_mode)

    # steps per epoch and validation steps set
    steps_per_epoch = _calc_steps_per_epoch(train_generator.samples, batch_size)
    validation_steps = _calc_steps_per_epoch(validation_generator.samples, batch_size)

    # train with fit_generator
    history = model.fit_generator(
        train_generator,
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        validation_data=validation_generator,
        validation_steps=validation_steps)

    # file naming strings
    now_string = file_util.date_string_now()

    # info for img size, epochs, conv2d layers, conv2d filters, and dropout
    info_string = str(img_width) + 'x' + str(img_height) + '_' + str(epochs) + '-e_' + str(
        conv_2d_layers) + '-c2dl_' + str(conv_2d_filters) + '-f_' + str(dropout) + '-d'

    # join file name string and info string
    file_string = now_string + '_' + info_string

    # show data
    if show:
        show_plot(history, file_string)

    # save data
    if save:
        save_model(model, file_string)

    return model


def run():
    train_directory = 'datasets/pokemon/train'
    validate_directory = 'datasets/pokemon/validate'

    img_width = 100
    img_height = img_width

    model = train(train_directory, validate_directory, img_width, img_height)
    get_predictions(model, 'examples', validate_directory, img_width, img_height)
    model_metrics(model, validate_directory, img_width, img_height)


# run()
