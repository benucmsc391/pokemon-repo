# README #
####Pokemon Image Recognition
Steve Schultz, Colom Boyle

Python version: 3.6

![result](media/image/demo_pika.gif)

### What is this repository for? ###
Benedictine University CMSC-391 Selected Topics (Machine Learning) Final Project. A Pokemon-based image recognition project using a Keras neural network to classify images. The trained neural network is 
implemented in a Flask web application where users may upload Pokemon images; The app will display the predicted type of Pokemon.

* Version: Alpha
* Project Overview:
    * Keras neural network trained to recognize several types of Pokemon.
        * Over 400 images used as a training/validation dataset.
        * Currently recognizable Pokemon: **Bulbasaur, Charmander, Pikachu, Squirtle.**
    * Python Flask web application implementing the trained neural network.
* Notable Libraries:
    * Keras (neural network), Flask, Bootstrap
    * Additional libraries listed in dependencies section.
* Flask app details: 
    * Image upload/recognition application:
        * User selects an image.
        * On image submission, image data is sent via AJAX.
        * Image data is run through the trained neural network.
        * Predicted Pokemon type and probability are displayed.
    * Visual design:
        * Bootstrap front-end.
        * JavaScript/HTML Canvas dynamic Poke Ball background.


### How do I get set up? ###

* Summary of set up
    * Ensure Python version 3.6 is in use. 
    * Install dependencies (listed below).
    * A new neural network does **not** need to be trained, as several trained models are located in the 'keras_model' directory.
    * Core image dataset is located in 'poke_dataset'.
    * 'datasets' directory contains copies of the images found in 'poke_dataset', divided into training and validation sets.
    
    
* Configuration
    * Core functionality is divided between neural network training code and the Flask image recognition application (keras_train.py and app.py). 
    
    * Trained neural network configurations save to the keras_model folder. Existing trained models are located in this folder. The web application uses one of these preexisting models to predict the class of an 
    uploaded image.
    
    * To train a new neural network, create a new Python file that imports keras_train.py and calls run(), or uncomment the run() function and run keras_train.py. NOTE: web app will not automatically use this new 
    network and must have its model file path updated in app.py.
    
    * To launch the web application, simply run app.py. The application uses one of the trained neural network files already present in the directory structure.

* Dependencies
    * Libaries: Keras, TensorFlow, numpy, sklearn, matplotlib, PIL, flask, and werkzeug.

* Know Bugs
    * Exception thrown when predicting image class.
        * Appears to occur when a neural network model trained on one device is used on another device. Consider training a new model and implementing the model for the web application. 

### Who do I talk to? ###

* Repo owner or admin
    * Steve Schultz or Colom Boyle